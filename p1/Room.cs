using System;

namespace p1
{

public class Room{

	//make data members private and getter/setter public

		private double length, width, height;
		private string type;

		//default constructor

		public Room()

		{
			type="Default";
			length = 10.0;
			width = 10.0;
			height= 10.0;
			Console.WriteLine("\nCreating  " + this.type + " room object from default constructor (accepts no arguments): ");
		}

			//parameterized constructor

		public Room(string t, double l, double w, double h)
		{
			length= l;
			width= w;
			height= h;
			type= t;

			Console.WriteLine("\nCreating  " + this.type + " room object from parameterized constructor (accepts no arguments): ");
		}

		public void SetLength(double l)
		{
			length = l;
		}

		public void SetWidth(double w)
		{
			width = w;
		}

		public void SetHeight(double h)
		{
			height= h;
		}

		public void SetType(string  t="default")
		{
			type= t;
		}

		public double GetLength()
		{
			return length;
		}
		public double GetWidth()
		{
			return width;
		}
		public double GetArea()
		{
			return length * width;
		}

		public double GetVolume()
		{
			return length * width * height;
		}

		public double GetHeight()
		{
			return height;
		}

		public string GetType()
		{
			return type;
		}
}
}