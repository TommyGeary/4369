using System;

namespace p1
{

public class RoomDemo
{
	public static void Main(String[] args)
	{

		string requirements= 

@"///////////////////////////////////
Program Requirements:
P1- Room Size Calculator using Classes
Author: Tommy Geary
1. Create Room class;
2. Create following fields (aka properties or data members)
	a. private string type: //room type
	b. private double length: //room length
	c. private double width: //room width
	d. private double height: //room height
3. Create two constructors:
	a. Default constructor
	b. Parameterized constructor that accepts four arguments (for fields above)
4. Create the following mutator (aka setter) methods:
	a. SetType
	b. SetLength
	c. SetWidth
	d. SetHeight
5. Create the following accessor (aka getter) methods:
	a. GetType
	b. GetLength
	c. GetWidth
	d. GetHeight
	e. GetArea
	d. GetVolume
6. Must include the following functionality: 
	a. display room size calculations in feet (as per below)
	b. Must include data validation
	c. Round to two decimal places
7. Allow user to press any key to return to command line. 
////////////////////////////////////// ";

		Console.WriteLine(requirements);

		DateTime localtime= DateTime.Now;
		Console.Write(localtime);

		Console.WriteLine();

		

		double width2=0.0, length2=0.0, height2=0.0, area2=0.0, volume2=0.0;
		string type2="";

		
		
		
		Room r1 = new Room();

		Console.WriteLine("Room type: " + string.Format("{0:0.00}", r1.GetType()));
		Console.WriteLine("Room length: " + string.Format("{0:0.00}", r1.GetLength()));
		Console.WriteLine("Room width: " + string.Format("{0:0.00}", r1.GetLength()));
		Console.WriteLine("Room area: " + string.Format("{0:0.00}", r1.GetArea()));
		Console.WriteLine("Room volume: " + string.Format("{0:0.00}", r1.GetVolume()));
		Console.WriteLine("Room volume in yards: " + string.Format("{0:0.00}",r1.GetVolume() /27,2 ) + " cu. yards");

		Console.Write("\nRoom Type: ");

		type2= Console.ReadLine();

		Console.Write("Room Length: ");

		while (!double.TryParse(Console.ReadLine(), out length2))

		{

			Console.WriteLine("Room Length Must be numeric. ");

		}


		Console.Write("Room Width: ");

		while (!double.TryParse(Console.ReadLine(), out width2))

		{

			Console.WriteLine("Room Width Must be numeric. ");
			
		}


		Console.Write("Room Height: ");

		while (!double.TryParse(Console.ReadLine(), out height2))

		{

			Console.WriteLine("Room Height Must be numeric. ");
			
		}

		Console.WriteLine("\nDisplay " + r1.GetType() + " room object's new data members. ");

			r1.SetType(type2);
			r1.SetLength(length2);
			r1.SetWidth(width2);
			r1.SetHeight(height2);

			Console.Write("Room Type: ");
			Console.WriteLine(r1.GetType());


			Console.Write("Room Length: ");
			Console.WriteLine(r1.GetLength());
			

			Console.Write("Room Width: ");
			Console.WriteLine(r1.GetWidth());
			

			Console.Write("Room Height: ");
			Console.WriteLine(r1.GetHeight());

			Console.Write("Room Area: ");
			Console.WriteLine(string.Format("{0:0.00}", r1.GetArea()));

			
			Console.Write("Room Volume: ");
			Console.WriteLine(string.Format("{0:0.00}", r1.GetVolume() + " cu. ft."));

			
			Console.Write("Room Volume(yards): ");
			Console.WriteLine(string.Format("{0:0.00}", r1.GetVolume() /27,2) + " cu. yards ");


			Console.WriteLine("\nCall Parameterized constructor (accepts 4 arguments): ");


			Console.Write("\nRoom Type: ");

			type2= Console.ReadLine();

			Console.Write("Room Length: ");

		while (!double.TryParse(Console.ReadLine(), out length2))

		{

			Console.WriteLine("Room Length Must be numeric. ");

		}


		Console.Write("Room Width: ");

		while (!double.TryParse(Console.ReadLine(), out width2))

		{

			Console.WriteLine("Room Width Must be numeric. ");
			
		}


		Console.Write("Room Height: ");

		while (!double.TryParse(Console.ReadLine(), out height2))

		{

			Console.WriteLine("Room Height Must be numeric. ");
			
		}

		Room r2= new Room(type2, length2, height2, width2);




			
			Console.Write("Room Type: ");
			Console.WriteLine(r2.GetType());


			Console.Write("Room Length: ");
			Console.WriteLine(r2.GetLength());
			

			Console.Write("Room Width: ");
			Console.WriteLine(r2.GetWidth());
			

			Console.Write("Room Height: ");
			Console.WriteLine(r2.GetHeight());

			Console.Write("Room Area: ");
			Console.WriteLine(string.Format("{0:0.00}", r2.GetArea() + " sq. ft. "));

			
			Console.Write("Room Volume: ");
			Console.WriteLine(string.Format("{0:0.00}", r2.GetVolume() + " cu. ft."));

			
			Console.Write("Room Volume(yards): ");
			Console.WriteLine(string.Format("{0:0.00}", r2.GetVolume() /27,2) + " cu. yards ");

				Console.WriteLine("\nPress any key to exit!");


          Console.ReadKey(true);

		


			}
}
}