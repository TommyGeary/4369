> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Tommy Geary

### Homework Links

[A1 Readme.md](a1/README.md) "Assignment 1 Readme"

* Distributed version control with Bitbucket
* Development installation
* Chapter 1-2 Questions


[A2 Readme.md](a2/README.md) "Assignment 2 Readme"

* Backward engineer screenshots
* Take screenshots and upload to Bitbucket
* Questions



[A3 Readme.md](a3/README.md) "Assignment 3 Readme"

* Backward engineer screenshot
* Take screenshots and upload to Bitbucket
* Questions



[P1 Readme.md](p1/README.md) "Project 1 Readme"

* Backward engineer screenshot
* Take screenshots and upload to Bitbucket
* Questions


[A4 Readme.md](a4/README.md) "Assignment 4 Readme"

* Backward engineer screenshot
* Take screenshots and upload to Bitbucket
* Questions


[A5 Readme.md](a5/README.md) "Assignment 5 Readme"


* Watch videos and learn how to use inheritance
* Take screenshots and upload to Bitbucket
* Questions


[P2 Readme.md](p2/README.md) "Project 2 Readme"

* Watch video 
* Complete tutorial
* Create Program

