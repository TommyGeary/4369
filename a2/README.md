> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Tommy Geary

### Assignment 2 Requirements:

*Three parts*

1. Backward engineer screenshots
2. Take screenshots and upload to Bitbucket
3. Questions

#### README.md file should include the following items:

* Screenshot of hwapp
* Screenshot of aspnetcoreapp [aspnetcoreapp](http://localhost:5000 "aspnetcoreapp")
* git commands with short description
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of valid result:

![Valid Screenshot](img/valid.png)

*Screenshot of invalid result*:

![Invalid Screenshot](img/invalid.png)



