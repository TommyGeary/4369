using System;
using System.Collections.Generic;
using System.Linq;

public class Program2
{
    public static void Main()
    {
        var people = GenerateListOfPeople();

        // Write your code here

        //There will be two Persons in this variable: the "Steve" Person and the "Jane" Person

        var peopleOverTheAgeOf30 = people.Where(x => x.LastName == "Smith");

        Console.WriteLine("Where: ");

        foreach(var person in peopleOverTheAgeOf30)

        {
           
            Console.WriteLine(person.FirstName); 
        }

        Console.WriteLine();

        //Will ignore Eric and Steve in the list of people
        IEnumerable<Person> afterTwo = people.Skip(2);

        Console.WriteLine("Skip: ");

        foreach(var person in afterTwo)
        {
            Console.WriteLine(person.FirstName);
        }

        Console.WriteLine();

        Console.WriteLine("Take: ");


        IEnumerable<Person> takeTwo = people.Take(2);
        foreach(var person in takeTwo)
        {
            Console.WriteLine(person.FirstName);
        }

        Console.WriteLine();


        Console.WriteLine("***Changing Each Item in Collections***\n");


        Console.WriteLine("Select: ");


        IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
        foreach(var firstName in allFirstNames)
        {
            Console.WriteLine(firstName);
        }

        Console.WriteLine();

        Console.WriteLine("Full name: ");

        IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
        foreach(var fullName in allFullNames)
        {
            Console.WriteLine($"{fullName.Last}, {fullName.First}");
        }

        Console.WriteLine();


        Console.WriteLine("***Finding One Item in Collections***\n");

        Console.WriteLine("FirstOrDefault: ");

        Person firstOrDefault = people.FirstOrDefault();
        Console.WriteLine(firstOrDefault.FirstName);

        Console.WriteLine();

        Console.WriteLine("FirstOrDefault as filter: ");

        var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age == 30);
        var firstThirtyYearOld2 = people.Where(x => x.Age == 30).FirstOrDefault();
        Console.WriteLine(firstThirtyYearOld1.FirstName); //Will output "Brendan"
        Console.WriteLine(firstThirtyYearOld2.FirstName); //Will also output "Brendan"

        Console.WriteLine();


        Console.WriteLine("How OrDefault works: ");

        List<Person> emptyList = new List<Person>();
        Person willBeNull = emptyList.FirstOrDefault();

        List<Person> people2 = GenerateListOfPeople();
        Person willAlsoBeNull = people2.FirstOrDefault(x => x.FirstName == "John"); 

        Console.WriteLine(willBeNull == null); // true
        Console.WriteLine(willAlsoBeNull == null); //true

        Console.WriteLine();

        Console.WriteLine("LastOrDefault as filter: ");

        Person lastOrDefault = people.LastOrDefault();
        Console.WriteLine(lastOrDefault.FirstName);
        Person lastThirtyYearOld = people.LastOrDefault(x => x.Age == 30);
        Console.WriteLine(lastThirtyYearOld.FirstName);

        Console.WriteLine();

        Console.WriteLine("SingleOrDefault as filter: ");
        
        Person single = people.SingleOrDefault(x => x.FirstName == "Eric"); 
        Console.WriteLine(single.FirstName);
        // Uncomment the next line to see it throw an exception
        // Person singleDev = people.SingleOrDefault(x => x.Occupation == "Dev");

        Console.WriteLine();

        Console.WriteLine("***Finding Data about Collections***\n");

        Console.WriteLine("Count(): ");

        int numberOfPeopleInList = people.Count();
        Console.WriteLine(numberOfPeopleInList);

        Console.WriteLine();

        Console.WriteLine("Count() with predicate expression: ");

        int peopleOverTwentyFive = people.Count(x => x.Age > 25);
        Console.WriteLine(peopleOverTwentyFive);

        Console.WriteLine();

        Console.WriteLine("Any: ");

        var emptyList2 = new List<Person>();

        bool thereArePeople = people.Any();
        Console.WriteLine(thereArePeople);
        bool thereAreNoPeople = emptyList2.Any();
        Console.WriteLine(thereAreNoPeople);

        Console.WriteLine();

        Console.WriteLine("All: ");

        bool allDevs = people.All(x => x.Occupation == "Dev");
        Console.WriteLine(allDevs);
        bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24);
        Console.WriteLine(everyoneAtLeastTwentyFour);

        Console.WriteLine();

        Console.WriteLine("***Converting Results to Collections***\n");

        Console.WriteLine("ToList(): ");

        List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList(); //This will return a List<Person>

        foreach (var person in listOfDevs)
        {

        Console.WriteLine(person.FirstName);

            }

        Console.WriteLine();

        Console.WriteLine("ToArray(): ");

        Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray(); //This will return a Person[] array

        foreach (var person in arrayOfDevs)
        {
            Console.WriteLine(person.FirstName);
        }

        Console.WriteLine();

        Console.WriteLine("***Required Program***\n");

        Console.Write("Enter a last name: ");

        string last = Console.ReadLine();
        int age=0;


        var name = people.Where(x => x.LastName == last);


        foreach(var person in name)

        {
           
            Console.WriteLine("Matching Criteria: " + person.FirstName + " " + person.LastName + " is a " + person.Occupation + " and is " + person.Age + " years old."); 
        }

        Console.WriteLine();

        Console.Write("Enter an age: ");

        while (!int.TryParse(Console.ReadLine(), out age))
        {

            Console.WriteLine("Age must be an integer.");
        }

        Console.Write("Enter an Occupation: ");

        string occ = Console.ReadLine();

        var age2= people.Where(x => x.Age == age);

            foreach(var person in age2.Where(x => x.Occupation == occ))
            {
                Console.WriteLine(person.FirstName + " " + person.LastName);
            }


    }

    public static List<Person> GenerateListOfPeople()
    {
        var people = new List<Person>();

        people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
        people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
        people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
        people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
        people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });

        return people;
    }
}

public class Person
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Occupation { get; set; }
    public int Age { get; set; }
}

public class FullName
{
    public string First { get; set; }
    public string Last { get; set; }
}