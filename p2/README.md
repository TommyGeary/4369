> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Tommy Geary

### Project 2 Requirements:

*Three parts*

1. Watch video
2. Follow tutorial
3. Create program

#### README.md file should include the following items:

* Screenshot of Main


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of valid result:*

![Valid Screenshot](img/valid.png)

)





