> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Tommy Geary

### Assignment 1 Requirements:

*Three parts*

1. Distributed version control with Bitbucket
2. Development installation
3. Chapter 1-2 Questions

#### README.md file should include the following items:

* Screenshot of valid program
* Screenshot of invalid invalid

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init -This command creates an empty Git repository - basically
2. git status -Displays paths that have differences between the index file and the current HEAD commit, paths that have differences between the working tree and the index file, and paths in the working tree that are not tracked by Git
3. git add -This command updates the index using the current content found in the working tree, to prepare the content staged for the next commit.
4. git commit -Stores the current contents of the index in a new commit along with a log message from the user describing the changes.
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git merge -Join two or more development histories together

#### Assignment Screenshots:

*Screenshot of hwapp:

![Readme.md Screenshot](img/hwapp.png)

*Screenshot of aspnetcoreapp*:

![A1 Readme Screenshot](img/aspnetcoreapp.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/TommyGeary/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/TommyGeary/myteamquotes/ "My Team Quotes Tutorial")

