using System;

namespace ConsoleApplication
{
    public class assignment3
    {
        public static void Main(string[] args)
        {
          
           string intro = @"///////////////////////////////
           Program Requirements:
           A3- Future Value Calculator
    Author: Tommy Geary
1. Use intrinsic method to display date;
2. Research: what is future value? and it's formula;
3. Create FutureValue method using the following parameters: decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep;
4. Initialize suitable variables: use decimal data types for currency variables;
5. Perform data validation: prompt user until correct data is entered;
6. Display money in currency format;
7. Allow user to press any key to return back to the command line.
///////////////////////////////////";

		   	Console.WriteLine(intro);
		   	DateTime localdate= DateTime.Now;
		   	Console.WriteLine("\n" + localdate);

		   	
		   	decimal balance= 0.0M;
		   	decimal interest= 0.0M;
		   	int years= 0;
		   	decimal future=0;
		   	decimal deposit= 0.0M;
		   	decimal pvalue= 0.0M;


		   	Console.Write("\nEnter a starting balance: ");

		   	while (!Decimal.TryParse(Console.ReadLine(), out balance))
		{
			Console.WriteLine("Starting Balance must be numeric.");
			Console.Write("Starting Balance:");

		}


		   	Console.Write("\n Enter a term in years: ");

		while (!int.TryParse(Console.ReadLine(), out years))
		{
			Console.WriteLine("Term must be an integer data type.");
			Console.Write("Enter the term in years: ");
			
		}


		   	Console.Write("\n Enter an interest rate: ");

		while (!Decimal.TryParse(Console.ReadLine(), out interest))
		{
			Console.WriteLine("Interest must be numeric.");
			Console.Write("Starting Balance:");
			
		}


		   	Console.Write("\n Enter a monthly deposit amount: ");

		while (!Decimal.TryParse(Console.ReadLine(), out deposit))
		{
			Console.WriteLine("Monthly deposit must be numeric.");
			Console.Write("Starting Balance:");
			
		}

		future= FutureValue(balance, years, interest, deposit);

		Console.WriteLine("********FutureValue********");
		Console.Write("{0:C}", future);



	}

	public static decimal FutureValue(decimal pvalue, int years, decimal interest, decimal deposit)
	{
		int months=years *12;
		double monthlyinterest=(double)interest/12/100;

		return pvalue * (decimal)Math.Pow((1 + monthlyinterest), months) + (deposit * (decimal)(Math.Pow((1 + monthlyinterest), months) -1)/(decimal)monthlyinterest);

	}
}
}
