using System;


    public class Student : Person
    {
       private string college;
       private string major;
       private double gpa;

       //default constructor
       public Student()
       {
       	college= "CCI";
       	major= "IT";
            gpa= 4.0;

       	//dont call an instance method in a constructor, as it's not yet fully initialized

       	Console.WriteLine("Two examples (inside default constructor): ");
       	Console.WriteLine("Creating default person object from default constructor(accepts no arguments): ");

       	Console.WriteLine("Creating " + this.fname + " " + this.lname + " person object from default constructor (accepts no arguments): ");


       }
       //paramaterized constructor, not default
       public Student(string fn="", string ln="", int a=0, string c="", string m= "", double g=0.0) : base (fn, ln, a)


       {

       	college= c;
       	major=m;
            gpa=g;

            Console.WriteLine("Creating derived student object from paramaterized constructor(accepts arguments)");


       }

       public void SetFname(string fn="")
       {
       	fname=fn;

       }

       public void SetLname(string ln="")
       {
       	lname=ln;

       }

       public void SetAge(int a=0)
       {
            age=0;;

       }

       //accessor shit

       public string GetFname()
       {
       	return fname;

       }

       public string GetName()
       {
       	return fname + "" + lname;
       }

       public override string GetObjectiveInfo()
       {

            return base.GetObjectiveInfo() + " in the college of " + this.college + ", majoring in " + this.major + ", and has a " + this.gpa.ToString("0.00") + " gpa.";
       }




	}
