using System;


    public class Person
    {
       protected string fname;
       protected string lname;
       protected int age;

       //default constructor
       public Person()
       {
       	fname= "john";
       	lname= "doe";
            age= 0;

       	//dont call an instance method in a constructor, as it's not yet fully initialized

       	Console.WriteLine("Two examples (inside default constructor): ");
       	Console.WriteLine("Creating default person object from default constructor(accepts no arguments): ");

       	Console.WriteLine("Creating " + this.fname + " " + this.lname + " person object from default constructor (accepts no arguments): ");


       }
       //paramaterized constructor, not default
       public Person(string fn="", string ln="", int a=0)

       {

       	fname= fn;
       	lname=ln;
            age=a;


       }

       public void SetFname(string fn="")
       {
       	fname=fn;

       }

       public void SetLname(string ln="")
       {
       	lname=ln;

       }

       public void SetAge(int a=0)
       {
            age=a;

       }

       //accessor shit

       public string GetFname()
       {
       	return fname;

       }

       public string GetLname()
       {
       	return lname;
       }

       public int GetAge()
       {
            return age;
       }

       public virtual string GetObjectiveInfo()
       {

            return fname + " " + lname + " " + age.ToString();
       }


	}
