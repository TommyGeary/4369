using System;

namespace a4
{

public class assignment4
{
	public static void Main(String[] args)
	{

		string requirements= 

@"///////////////////////////////////
Program Requirements:
Using Class Inheritance
Author: Tommy Geary

1. Create Person class (must be stored in a discrete file apart from main method)
	
	a. Create three protected data members: 
		1. fname
		2. lname
		3. age 
	b. Create three setter/mutator methods:
		1. SetFname
		2. SetLname
		3. SetAge
	c. Create four getter/accessor methods:
		1. GetFname
		2. GetLname
		3. GetAge
		4. GetObjectInfo (virtual method (returns string): allows derived class to override base class method)

Note: for this excecise, do NOT use shorthand/shortcut get/set methods.

	d. Create two constructors:
		1. default constructor (accepts no arguments)
		2. parameterized constructor
	e. Instantiate two person objects
		1. from default constructor
		2. from parameterized constructor

2. Create Student class (must be stored in discrete file apart from main method)

	a. Create three private data members
		1. college
		2. major
		3. gpa
	b. Create three getter/accessor methods
		1. GetName 
		2. GetFullName 
		3. GetObjectInfo (demonstrates polymorphism: one interface, multiple roles/purposes)

Note: for this exercise, do NOT use shorthand/shortcut get/set methods.

	c. Create two constructors
		1. default constructor (accepts no arguments)
		2. parameterized constructor 
	d. Instantiate two student objects
		1. from default constructor
		2. from parameterized constructor

3. Allow users to press any key to return to console.";

Console.WriteLine(requirements);

		DateTime localtime= DateTime.Now;
		Console.Write(localtime);

		Console.WriteLine();


		string pfirstName="";
		string plastName="";
		int page=0;
		string sfirstName="";
		string slastName="";
		int sage=0;
		string scollege= "";
		string smajor= "";
		double sgpa= 0.0;

	 Person person1= new Person();



	 Console.Write("First Name: " + person1.GetFname() + "\n");

	 Console.Write("Last Name: " + person1.GetLname() + "\n");

	 Console.Write("Age: " + person1.GetAge() + "\n");


	 Console.WriteLine("\nModify default constructor object's data member values");
	 Console.WriteLine("Using setter getter methods");

	 	
	 	Console.Write("First name: ");
	 	 	 pfirstName= Console.ReadLine();

	 	 	 	 Console.Write("Last Name: ");
				plastName= Console.ReadLine();

				Console.Write("Age: ");
		
		while (!int.TryParse(Console.ReadLine(), out page))
		{
			Console.WriteLine(" Age must be an integer.");
		}



			person1.SetFname(pfirstName);
			person1.SetLname(plastName);
			person1.SetAge(page);

			Console.WriteLine("\n Display object's new data member values: ");

	 	 	
	 	 	 Console.Write("First Name: " + person1.GetFname() + "\n");
	 	 	 	Console.Write("Last Name: " + person1.GetLname() + "\n");
	 	 	 	Console.Write("Age: " + person1.GetAge() + "\n");

	 	 	 


			Console.WriteLine("\nCall Parameterized constructor (accepts two arguments): ");

			Console.Write("First name: ");
	 	 	 pfirstName= Console.ReadLine();

	 	 	 	 Console.Write("Last Name: ");
				plastName= Console.ReadLine();

				Console.Write("Age: ");

				while (!int.TryParse(Console.ReadLine(), out page))
		{
			Console.WriteLine(" Age must be an integer.");
		}


			Person person2= new Person(pfirstName, plastName, page);

			Console.Write("First name: " + person2.GetFname() + "\n");

			Console.Write("Last name: " + person2.GetLname() + "\n");

			Console.Write("Age: " + person2.GetAge() + "\n");

			Console.WriteLine("\n Create Person object from parameterized constructor(accepts two arguments): ");

			Console.Write("Full name: "+ person2.GetFname() + " " + person2.GetLname() + " Age: " + person2.GetAge() );



			Student student1 = new Student();

			
			Console.Write("First name: " + student1.GetFname() + "\n");

			Console.Write("Last name: " + student1.GetLname() + "\n");

			Console.Write("Age: " + student1.GetAge() + "\n");

			Console.WriteLine("\nDemonstrating polymorphism (new derived object): ");
			Console.WriteLine("(Calling paramterized base class constructor explicitly)");



			Console.Write("First name: ");
	 	 	 sfirstName= Console.ReadLine();

	 	 	 	 Console.Write("Last Name: ");
				slastName= Console.ReadLine();

				Console.Write("Age: ");

				while (!int.TryParse(Console.ReadLine(), out sage))
		{
			Console.WriteLine(" Age must be an integer.");
		}

			Console.Write("College: ");
			scollege= Console.ReadLine();

			Console.Write("Major: ");
			smajor= Console.ReadLine();

			Console.Write("GPA: ");
			while (!double.TryParse(Console.ReadLine(), out sgpa))
		{
			Console.WriteLine(" GPA must be a numberic value.");
		}

		Student student2 = new Student(sfirstName, slastName, sage, scollege, smajor, sgpa);

		Console.WriteLine();

		Console.Write("person2 -GetObjectInfo (virtual): \n");

		Console.WriteLine(person2.GetObjectiveInfo());


		Console.Write("\nstudent2 -GetObjectInfo (overriden): \n");

		Console.WriteLine(student2.GetObjectiveInfo());

		Console.WriteLine();

		Console.WriteLine("Press any key to exit");

		Console.ReadKey();




}

}

}