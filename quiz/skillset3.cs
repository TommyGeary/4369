﻿using System;

namespace ConsoleApplication
{
    public class skillset3
    {
        public static void Main(string[] args)
        {
          
           string intro = @"Program Requirements:
           1. Use intrinsic method to display date/time;
           2. use two types of selection structures: if...else if and switch;
           3. Initialize suitable variables: use decimal data type for bill;
           4. Prompt and capture user input
           5. Allow user to press any key to return back to command line";

           Console.Write( intro );

           DateTime localdate= DateTime.Now;

           Console.Write( localdate );

           Console.Write(" If... else Example");

           Console.WriteLine( " Your Bill is $100.00 ");

           Console.WriteLine( "Discounts: ");
           Console.WriteLine( "6+: 30% off ");
           Console.WriteLine( "3-5: 20% off ");
           Console.WriteLine( "2: 10% off ");
           Console.WriteLine( "1: regular price ");

           Console.Write( "How many books did you buy?: \n");

           int book = Convert.ToInt32(Console.ReadLine());

           double price= 100.00;

           if ( book == 1)

           {

            price = price; 

           }
           
           else if ( book ==2)
           {

            price= price * .9;
           }

           else if (book >=3 && book <=5)
           {

            price= price * .8;

           }

           else 

           {

           price= price * .7;

         }

         Console.WriteLine("Your cost is " + " $" + price + "\n");


         Console.WriteLine(" Switch Example\n");

        Console.WriteLine(" 1 - red ");
        Console.WriteLine(" 2 - blue ");
        Console.WriteLine(" 3 - green ");
        Console.WriteLine(" 4 - black ");

        int color = Convert.ToInt32(Console.ReadLine());

        switch (color)

        {
          case 1:
          Console.WriteLine("Your Favorite Color is Red!\n");
          break;

          case 2:
          Console.WriteLine("Your Favorite Color is Blue!\n");
          break;

          case 3:
          Console.WriteLine("Your Favorite Color is Green!\n");
          break;

          case 4:
          Console.WriteLine("Your Favorite Color is Black!\n");
          break;

          default:
        }



          

          Console.ReadKey(true);



        }
    }
}