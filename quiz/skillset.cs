﻿using System;

namespace ConsoleApplication
{
    public class skillset1
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Program Requirements: ");

            Console.WriteLine("  1) Initialize suitable variable(s);");
            Console.WriteLine("  2) Prompt and capture user input;");
            Console.WriteLine("  3) Display user input;");
            Console.WriteLine("  4) Allow user to press any key to return back to command line");

          Console.Write("Please enter your full name: ");
          string name = Console.ReadLine();
          Console.WriteLine("Hello " + name + "!");

          Console.WriteLine("Press any key to exit!");
          Console.ReadKey();



        }
    }
}