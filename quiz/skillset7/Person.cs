using System;


    public class Person
    {
       private string fname;
       private string lname;

       //default constructor
       public Person()
       {
       	fname= "john";
       	lname= "doe";

       	//dont call an instance method in a constructor, as it's not yet fully initialized

       	Console.WriteLine("Two examples (inside default constructor): ");
       	Console.WriteLine("Creating default person object from default constructor(accepts no arguments): ");

       	Console.WriteLine("Creating " + this.fname + " " + this.lname + " person object from default constructor (accepts no arguments): ");


       }
       //paramaterized constructor, not default
       public Person(string fn="", string ln="")

       {

       	fname= fn;
       	lname=ln;


       }

       public void SetFname(string fn="")
       {
       	fname=fn;

       }

       public void SetLname(string ln="")
       {
       	lname=ln;

       }

       //accessor shit

       public string GetFname()
       {
       	return fname;

       }

       public string GetLname()
       {
       	return lname;
       }
	}
