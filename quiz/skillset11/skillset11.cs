using System;

namespace skillset
{

public class skillset11
{
	public static void Main(String[] args)
	{

		string requirements= 

@"Program requirements: 
Methods: Pass by value vs pass by reference
Author: Tommy Geary

1. Create two methods:
	a. PassByVal
	b. PassByRef

2. Initialize one (double) variable: num1

3. Allow user input and data validate.

4. Allow user to press any key to return back to command line
///////////////////////////////////////////////";

Console.WriteLine(requirements);

		DateTime localtime= DateTime.Now;
		Console.Write(localtime);

		Console.WriteLine();

		double num1= 0.0;


	Console.WriteLine("Please enter a number: ");

	while(!double.TryParse(Console.ReadLine(), out num1))
	{
		Console.WriteLine("Number must be a numeric value.");
	}


	Console.WriteLine("Call PassByVal().");
	PassByVal(num1);
	Console.WriteLine("Inside main num1= " + num1 + "\n");
	Console.WriteLine("Call PassByRef() ");
	PassByRef(ref num1);
	Console.WriteLine("Inside main num1= " + num1 + "\n");

}
	public static void PassByVal(double stuff)
	{
		Console.WriteLine("Inside PassByVal() method");
		stuff= stuff + 5;
		Console.WriteLine("Added 5 to number passed by value = " + stuff);

		Console.WriteLine("Call PassByRef().");
	}


	public static void PassByRef(ref double otherstuff)
	{
		Console.WriteLine("Inside PassByVal() method");
		otherstuff= otherstuff + 5;
		Console.WriteLine("Added 5 to number passed by value = " + otherstuff);

	}

}
}







