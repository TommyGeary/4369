using System;

namespace a5{



public class Vehicle
{

	private float milesTraveled, gallonsUsed;
	private int year;

	//testing making properties private

	public string Make {get; set;}
	public string Model {get; set;}


	//read-only property- no set method
	public float MPG
	
	{
		get {
			if (gallonsUsed <= 0.0f)
			{

				return 0.0f;
			}
			else 
			{
				return milesTraveled / gallonsUsed;
			}

		}
		//set {...} method not used
	}

	public Vehicle()
	{
		this.Make= "Chevy";
		this.Model="Camaro";
		this.year= 1999;

		Console.WriteLine("\nCreating base object from default constructo (accepts no args): ");


	}

	//parameterized constructor
	//demo by commenting out default constructor

	public Vehicle(string mk="Make", string md= "Model", int yr=0)
	{

		this.Make=mk;
		this.Model=md;
		this.year=yr;

		Console.WriteLine("\nCreating base object from parameterized constructor (accepts arguments): ");

	}

	public void SetMiles(float m= 0.0f)
	{
		milesTraveled=m;
	}

	public void SetGallons(float g=0.0f)
	{
		gallonsUsed=g;
	}

	public float GetMiles()
	{
		return milesTraveled;

	}
	
	public float GetGallons()
	{
		return gallonsUsed;
	}

	public virtual string GetObjectInfo()
	{
		return Make + " - " + Model + " - " + year + " - " + MPG;
	}

	public virtual string GetObjectInfo(string sep)
	{
		return Make + sep + Model + sep + year + sep + MPG;
	}
}
}