using System;

namespace a5
{
	public class assignment5
	{

		public static void Main(String[] args)
		{

			string requirements=
@"////////////////////////////////
Program requirements:
Using Classes - Inheritance
Also, Overloading (Compile-Time Polymorphism) and Overriding (Run-Time Polymorphism)
Author: Tommy Geary

*NB:* Vehicle and Car class definitions must be stored in discrete files apart from Main() method.

A. Create Vehicle (base/super/parent) class

1. Create two private data members (fields/instance variables):
	a. milesTraveled (float)
	b. gallonsUsed (float)

2. Create four properties
	a. Manufacter (string)
	b. Make (string)
	c. Model (string)
	d. MPG (float)
Note: Only manufactuer, Make, and Model are auo-implemented properties (MPG is read-only).

3. Create two setter/mutator methods
	a. SetMiles
	b. SetGallons
	
4. Create four getter/accessor methods:
	a. GeMiles
	b. GetGallons
	c. GetObjectInfo()
	d. GetObjectInfo(arg) (overload method): accepts string arg to be used as a separator for display purposes.

5. Create two constructors:
	a. defualt constructor
	b. parameterized constructor w/ default parameter values that accepts three arguments

B. Create Car (derived/child/sub) class
	1. Create one private data member (field/instance variable):
		a. style (string)
	2. Create one property:
		a. Style(string)
		Note: Stle property is not auto auo-implemented
	3. Create two constructors:
		a. defualt constructor
		b. parameterized constructor
	4. Create one getter/accessor method:
		a. GetObjectInfo(arg) (overridden method): accepts string arg to be used as a separator for display purposes

C. Instantiate three vehicle objects:
	 1. One from default constructor, display data member values
	 2. two from parameterized constructor (passing arguments to its constructor's parameters), display data member values

D. Instantiate one car object:
	1. One from parameterized constructor (passing arguments to its constructor's parameters), display data member values

E. Must include data validation on numeric inputs.

F. Allow user to press any key to return back to command line.
///////////////////////////////////////////";

Console.WriteLine(requirements);

		DateTime localtime= DateTime.Now;
		Console.Write(localtime);

		Console.WriteLine();

		Console.WriteLine("\nVehcile 1 (instantiating new object from default constructor");
			Vehicle vehicle1= new Vehicle();

			Console.WriteLine(vehicle1.GetObjectInfo());

			//call parameterized constructor
			Console.WriteLine("\nVehicle 2 (user input): Call parameterized base construcotr (accepts arguments):");

			string model2="";
			string make2="";
			int year2=0;
			float miles2=0.0f;
			float gallons2=0.0f;
			string separator2= " ";

			Console.Write("Make (alpha): ");
			make2= Console.ReadLine();


			Console.Write("Model (alpha): ");
			model2=Console.ReadLine();

			Console.Write("Year (int): ");

			while (!int.TryParse(Console.ReadLine(), out year2))
			{
				Console.Write("Year must be numeric");
			}


			Console.Write("Miles Driven (float): ");
			
			while (!float.TryParse(Console.ReadLine(), out miles2))
			{
				Console.Write("Miles must be numeric");
			}

			Console.Write("Gallons used (float): ");

			while (!float.TryParse(Console.ReadLine(), out gallons2))
			{
				Console.Write("Gallons must be numeric");
			}

			Console.WriteLine("\nVehicle 2 (instantiating new object, passing only 1st arg)\n" + " Demos why we use constructor with default parameter values): ");

			Console.WriteLine("Note: only 1st argument passed to constructor, others are default values.");

			Vehicle vehicle2= new Vehicle(make2);

			Console.WriteLine(vehicle2.GetObjectInfo());

			Console.WriteLine("\nUser input: vehicle2 -passing arg to overloaded GetObjectInfo(arg):");
			Console.Write("Delimiter(, ; ;): ");

			separator2=Console.ReadLine();

			Console.WriteLine(vehicle2.GetObjectInfo(separator2));

			Console.WriteLine("\nvehicle3 (instantiating new object, passing all args): ");

			Vehicle vehicle3 = new Vehicle(make2, model2, year2);

			vehicle3.SetGallons(gallons2);
			vehicle3.SetMiles(miles2);
			//float mpg=vehicle3.mpg

			Console.WriteLine("\nUser input: vehicle3 passing arg to overloaded (same scope, same class) GetObjectInfo(arg)");

			Console.Write("Delimiter(, ; ;): ");

			separator2=Console.ReadLine();
			Console.WriteLine(vehicle3.GetObjectInfo(separator2));

			Console.WriteLine("\nDemonstrating polymorphism (new derived object):");
			Console.WriteLine("(User input: car1 - calling parameterized base class constructor explicitly.)\n" );

			string style2="";

			Console.Write("Make (alpha): ");
			make2= Console.ReadLine();

			Console.Write("Model (alpha): ");
			model2= Console.ReadLine();


			Console.Write("Year (int): ");

			while (!int.TryParse(Console.ReadLine(), out year2))
			{
				Console.WriteLine("Year must be numeric");
			}

			Console.Write("Miles driven (float): ");

			while (!float.TryParse(Console.ReadLine(), out miles2))
			{
				Console.Write("Miles must be numeric");
			}

			Console.Write("Gallons used (alpha): ");

			while (!float.TryParse(Console.ReadLine(), out gallons2))
			{
				Console.Write("Gallons must be numeric");
			}

			Console.Write("Style (alphanumeric): ");
			style2=Console.ReadLine();

			Console.WriteLine("\ncar1 (instantiating new derived object passing all args): ");

			Car car1= new Car(make2, model2, year2, style2);

			car1.SetGallons(gallons2);
			car1.SetMiles(miles2);

			Console.WriteLine("\nUser input: car1 passing arg to overridden (different scope - inheritance) GetObjectInfo(arg):");



			Console.Write("Delimiter(, ; ;): ");

			separator2= Console.ReadLine();

			Console.WriteLine(car1.GetObjectInfo(separator2));

			Console.WriteLine();

			Console.WriteLine("Press any key to exit");

		Console.ReadKey();




		}
	}
}