using System;

namespace a5
{


public class Car : Vehicle
{
	private string style;

	public string Style
	{

		get {return style;}
		set {style = value;}

	}

	public Car()
	{
		this.Style= "default style";

		Console.WriteLine("\nCreating derived object from default constructor:");

	}

	public Car(string mk="Make", string md="Model", int yr=0, string st="Style")
	: base (mk, md, yr)
	{
		this.Style=st;

		Console.WriteLine("\nCreating base object from parameterized constructor (accepts arguments): ");

	}

	//overridden method: same name, same signature, different scope (inheritance)
	public override string GetObjectInfo(string sep)
	{
		//calls overloaded method from base class
		return base.GetObjectInfo(sep) + sep + Style;

	}
}
}