> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Tommy Geary

### Assignment 5 Requirements:

*Three parts*

1. Watch videos and learn how to use inheritance
2. Take screenshots and upload to Bitbucket
3. Questions

#### README.md file should include the following items:

* Screenshot of Main


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of valid result:*

![Valid Screenshot](img/valid.png)

)





